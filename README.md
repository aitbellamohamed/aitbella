# Very short description of the package

[![Latest Version on Packagist](https://img.shields.io/packagist/v/aitbella/cosmed.svg?style=flat-square)](https://packagist.org/packages/aitbella/cosmed)
[![Total Downloads](https://img.shields.io/packagist/dt/aitbella/cosmed.svg?style=flat-square)](https://packagist.org/packages/aitbella/cosmed)
![GitHub Actions](https://github.com/aitbella/cosmed/actions/workflows/main.yml/badge.svg)

This is where your description should go. Try and limit it to a paragraph or two, and maybe throw in a mention of what PSRs you support to avoid any confusion with users and contributors.

## Installation

You can install the package via composer:

```bash
composer require aitbella/cosmed
```

## Usage

```php
// Usage description here
```

### Testing

```bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email modfayz@gmail.com instead of using the issue tracker.

## Credits

-   [modfayz](https://github.com/aitbella)
-   [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

## HET THE PACKAGE FROM packagist WEBSITE

[packagist](https://packagist.org/packages/aitbella/cosmed)
